package com.training3.imdb_app.Activities;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.training3.imdb_app.Adapter.Favourite_adapter;
import com.training3.imdb_app.Getter_Setter.Result;
import com.training3.imdb_app.Getter_Setter.Wishlist_model;
import com.training3.imdb_app.R;
import com.training3.imdb_app.database.DatabaseHelper;
import java.util.ArrayList;
import java.util.List;

public class Favourite_acivity extends AppCompatActivity {

    private DatabaseHelper database;
    private RecyclerView recyclerView;
    private Favourite_adapter favourite_adapter;
    private TextView txt_messgae;
    public static List<Wishlist_model> datamodel = new ArrayList<Wishlist_model>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_acivity);

        setTitle("Favourite Movies");
        recyclerView=findViewById(R.id.recyclerview_show);
        txt_messgae=findViewById(R.id.txt_addfav);

        database = new DatabaseHelper(Favourite_acivity.this);
        datamodel=  database.getdata();

        if (datamodel.size()==0)
        {
            txt_messgae.setVisibility(View.VISIBLE);
        }
        else
        {
            txt_messgae.setVisibility(View.GONE);
        }
        favourite_adapter =new Favourite_adapter(this,datamodel);
        RecyclerView.LayoutManager reLayoutManager =new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(reLayoutManager);
        recyclerView.setAdapter(favourite_adapter);
    }

    @Override
    public void onRestart()
    {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }


}
