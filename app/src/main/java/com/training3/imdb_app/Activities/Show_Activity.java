package com.training3.imdb_app.Activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.training3.imdb_app.Getter_Setter.Wishlist_model;
import com.training3.imdb_app.R;
import com.training3.imdb_app.database.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

public class Show_Activity extends AppCompatActivity {


    private String get_image, get_title, get_releaseyear, get_overview, get_rating, get_id, get_votecount, get_voteaverage;
    private TextView txt_title, txt_releaseyear, txt_rating, txt_overview;
    private ImageView img_poster, img_favourite, img_fav_red;
    private Boolean aBoolean = false;
    private Button btn_review, btn_trailor;
    private List<Wishlist_model> idvalue = new ArrayList<Wishlist_model>();
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_);

        setTitle("PopularMovies");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        databaseHelper = new DatabaseHelper(this);

        getting_data();
        findviewid();
        set_data();
        clicklistiner();

        idvalue = databaseHelper.getdata();

        if (idvalue.size() == 0) {

        } else {

            for (int i = 0; i < idvalue.size(); i++) {
                if (get_id.equals(idvalue.get(i).getId())) {
                    aBoolean = true;
                }
            }
        }
        if (aBoolean) {
            img_favourite.setVisibility(View.GONE);
            img_fav_red.setVisibility(View.VISIBLE);
        }
    }


    private void getting_data() {

        get_image = getIntent().getStringExtra("image");
        get_title = getIntent().getStringExtra("title");
        get_releaseyear = getIntent().getStringExtra("releaseyear");
        get_overview = getIntent().getStringExtra("overview");
        get_rating = getIntent().getStringExtra("rating");
        get_id = getIntent().getStringExtra("id");
        get_votecount = getIntent().getStringExtra("votecount");
        get_voteaverage = getIntent().getStringExtra("voteaverage");
    }

    private void findviewid() {

        txt_title = findViewById(R.id.txt_title);
        txt_releaseyear = findViewById(R.id.txt_year);
        txt_rating = findViewById(R.id.txt_rating);
        txt_overview = findViewById(R.id.txt_description);
        img_poster = findViewById(R.id.img_posterimg);
        img_favourite = findViewById(R.id.img_fav);
        img_fav_red = findViewById(R.id.img_fav_red);
        btn_review = findViewById(R.id.btn_review);
        btn_trailor = findViewById(R.id.btn_trailor);

    }

    private void set_data() {

        txt_title.setText(get_title);
        if (get_image != null) {

            Picasso.with(this)
                    .load("https://image.tmdb.org/t/p/w500" + get_image).fit()
                    .placeholder(R.mipmap.ic_launcher)
                    .into(img_poster);

        }
        txt_releaseyear.setText(get_releaseyear);
        txt_rating.setText(get_rating);
        txt_overview.setText(get_overview);
    }

    private void clicklistiner() {

        img_favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isStoragePermissionGranted())
                {
                    savedata();
                }


            }
        });


        img_fav_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isStoragePermissionGranted())
                {
                    delete_data();
                }

            }
        });

        btn_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(Show_Activity.this);
                dialog.setContentView(R.layout.dialog_layout);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(dialog.getWindow().getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                dialog.show();
                dialog.getWindow().setAttributes(lp);

                Float getrating = Float.valueOf(get_voteaverage);

                final Float ggetrating = getrating / 2;

                TextView txt_title = dialog.findViewById(R.id.movie_title);
                TextView txt_votes = dialog.findViewById(R.id.txt_votenumber);
                RatingBar ratingBar = dialog.findViewById(R.id.ratingbar);
                txt_title.setText(get_title);
                txt_votes.setText(get_votecount);
                ratingBar.setRating(ggetrating);


            }
        });

        btn_trailor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isPackageInstalled("com.google.android.youtube", getPackageManager()))

                {
                    Intent intent = new Intent(Intent.ACTION_SEARCH);
                    intent.setPackage("com.google.android.youtube");
                    intent.putExtra("query", get_title + " trailer");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {

                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.google.android.youtube")));
                    } catch (android.content.ActivityNotFoundException anf) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + "com.google.android.youtube")));
                    }

                }

            }
        });
    }


    private void savedata()
    {

        databaseHelper.insertdata(get_id, get_image, get_title, get_releaseyear, get_rating, get_overview,get_votecount,get_voteaverage);
        img_favourite.setVisibility(View.GONE);
        img_fav_red.setVisibility(View.VISIBLE);

    }

    private void delete_data()
    {
        databaseHelper.delete(get_id);

        img_fav_red.setVisibility(View.GONE);
        img_favourite.setVisibility(View.VISIBLE);


    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager) {
        try {
            packageManager.getPackageGids(packagename);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onBackPressed() {


        super.onBackPressed();
    }


    public  boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        else {

            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0]== PackageManager.PERMISSION_GRANTED){

            savedata();

        }
    }
}
