package com.training3.imdb_app.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.training3.imdb_app.Activities.Show_Activity;
import com.training3.imdb_app.Getter_Setter.Result;
import com.training3.imdb_app.Getter_Setter.Wishlist_model;
import com.training3.imdb_app.R;

import java.util.List;

public class Favourite_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1;
    private Activity mActivity;
    private LayoutInflater mInflater;
    public static List<Wishlist_model> data;
    private Wishlist_model finallist;

    public Favourite_adapter(Activity mActivity, List<Wishlist_model> data) {
        this.mActivity = mActivity;
        mInflater = LayoutInflater.from(mActivity);
        this.data = data;


    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_ITEM;
        } else {
            return TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.movies_layout, parent, false);
        return new Favourite_adapter.FindHolder(view);
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof Favourite_adapter.FindHolder) {

            final String get_image, get_title, get_releaseyear, get_rating, get_overview,get_id,get_count,get_average;
            finallist = null;
            finallist = (Wishlist_model) data.get(position);

            get_image = finallist.getImage();
            get_title = finallist.getTitle();
            get_releaseyear = finallist.getYear();
            get_rating = finallist.getRating();
            get_overview = finallist.getOverview();
            get_id=finallist.getId().toString();
            get_count=finallist.getCount().toString();
            get_average=finallist.getAverage().toString();
            if (get_image != null) {

                Picasso.with(mActivity)
                        .load("https://image.tmdb.org/t/p/w500"+get_image)
                        .placeholder(R.mipmap.ic_launcher)
                        .into(((FindHolder) holder).imageView);

            }



            ((FindHolder) holder).imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mActivity, Show_Activity.class);
                    intent.putExtra("title", get_title);
                    intent.putExtra("image", get_image);
                    intent.putExtra("releaseyear", get_releaseyear);
                    intent.putExtra("rating", get_rating);
                    intent.putExtra("overview", get_overview);
                    intent.putExtra("id", get_id);
                    intent.putExtra("votecount", get_count);
                    intent.putExtra("voteaverage", get_average);

                    mActivity.startActivity(intent);
                }
            });

//            holder.imageView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//
//
//                }
//            });

        }
    }

    @Override
    public int getItemCount() {

        if (data == null) {
            return 0;
        }
        return data.size();
    }


    public class FindHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;

        public FindHolder(View v) {
            super(v);

            imageView = v.findViewById(R.id.img_poster);

        }

    }


}

